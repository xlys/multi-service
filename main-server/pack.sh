#!/bin/bash

now_path=$(pwd)
for dir in $(find ../ -maxdepth 3 -type d -name client -o -name server)
do
  dir_name=${dir%/*}
  dir_name=${dir_name##*/}
  mkdir -p ./projects/$dir_name
  if [[ $dir =~ "client" ]]
  then
    compress_name="${dir_name}_client.tar.gz"
    cd $dir
    npm install
    npm run build
    # cd $dir/dist
    tar -czvf $compress_name --exclude=node_modules -C dist/ .
    cd $now_path
    mv -f $dir/$compress_name ./projects/$dir_name
  else
    compress_name="${dir_name}_server.tar.gz"
    cd $dir
    tar -czvf $compress_name --exclude=node_modules ./
    cd $now_path
    mv -f $dir/$compress_name ./projects/$dir_name
  fi
done
tar -czvf ./school_plant.tar.gz --exclude=node_modules --exclude=.vscode --exclude=yarn.lock --exclude=modules.deploy.json ./
# 1.先在各自项目下打包，然后copy到project下
# 2.将本项目再次打包
# 3.根据用户配置动态解压对应业务模块 npm install 安装依赖并启动进程