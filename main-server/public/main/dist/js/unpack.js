const fs = require("fs");
const path = require("path")
const child_process = require("child_process");
const compressing = require("compressing");
// 没加path 所以文件不可随意
let modelArr = [];
let AllModel = true;
if (fs.existsSync("../../../../modules.deploy.json")) {
  AllModel = false;
  modelArr = fs.readFileSync("../../../../modules.deploy.json", {
    encoding: "utf8"
  });
} else {
  modelArr = fs.readFileSync("../../../../modules.config.json", {
    encoding: "utf8"
  });
}
modelArr = JSON.parse(modelArr);
function startPro(url, dir) {
  // child_process.exec("npm install", { cwd: url }, error => {
  //   if (error) {
  //     throw error;
  //   }
  // });
  // child_process.exec("npm run prd", { cwd: url }, error => {
  //   if (error) {
  //     throw error;
  //   } else {
  //     wirteDeploySync(dir)
  //   }
  // });
  child_process.execSync("npm install", { cwd: url })
  child_process.execSync("npm run prd", { cwd: url })
  wirteDeploySync(dir)
}
function unPack(fileName, dir) {
  const url = `../../../../public/${dir}`;
  if (!fs.existsSync(url)) {
    fs.mkdirSync(url);
  }
  const packPath = `../../../../projects/${dir}/${fileName}`;
  if (fileName.indexOf("client") !== -1) {
    // client
    const staticPath = "../../../../public/" + dir;
    compressing.tgz.uncompress(packPath, staticPath).catch(err => {
      console.log(err);
    });
  } else {
    // server;
    const projectPath = "../../../../projects/" + dir;
    compressing.tgz
      .uncompress(packPath, projectPath)
      .then(() => {
        startPro(projectPath, dir);
      })
      .catch(err => {
        console.log(err);
      });
  }
}
function readDirSync(path) {
  const pa = fs.readdirSync(path);
  pa.forEach(obj => {
    var info = fs.statSync(path + "/" + obj);
    if (info.isDirectory()) {
      readDirSync(path + "/" + obj);
    } else {
      const fileName = obj.substring(0, obj.indexOf("_"));
      modelArr.forEach(pro => {
        if (pro.key === fileName) {
          if (AllModel) {
            if (pro.base) {
              unPack(obj, fileName);
            }
          } else if (!pro.base) {
            unPack(obj, fileName);
          }
        }
      });
    }
  });
}
// 启动成功的进程写入deploy文件，添加状态start为true
function wirteDeploySync(k) {
  let depoly_path = path.join(__dirname, '../../../../modules.deploy.json')
  let deploy_data = JSON.parse(fs.readFileSync(depoly_path))
  deploy_data.map((item) => {
    item.newest = false
    if (item.key === k) {
      item.newest = true
      item.start = true
    }
  })
  fs.writeFileSync(depoly_path, JSON.stringify(deploy_data))
}
readDirSync("../../../../projects");
