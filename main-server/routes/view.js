const router = require('koa-router')()
const fs = require('fs')
const path = require('path')
const fileUrl = path.join(__dirname, '../modules.deploy.json')
if (fs.existsSync(fileUrl)) {
  const data = JSON.parse(fs.readFileSync(fileUrl))
  data.map((item)=> {
    router.get(`/${item.key}`, async (ctx, next) => {
      let htmlUrl = path.join(__dirname, `../public/${item.key}/index.html`)
      let html = fs.readFileSync(htmlUrl)
      ctx.response.type = 'html'
      ctx.response.body = html
    })
  })
} else {
  router.get('/', async (ctx, next) => {
    await next()
  })
}

module.exports = router