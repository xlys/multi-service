import Vue from 'vue'
import Router from 'vue-router'
Vue.use(Router)

export default new Router({
  routes: [
    {
      path: '/video/preview',
      component: (resolve) => {
        require(['../components/Preview.vue'], resolve)
      }
    },
    {
      path: '/video/playBack',
      component: (resolve) => {
        require(['../components/PlayBack.vue'], resolve)
      }
    },
    {
      path: '/video',
      component: (resolve) => {
        require(['../components/Preview.vue'], resolve)
      }
    },
    {
      path: '/',
      component: (resolve) => {
        require(['../components/Preview.vue'], resolve)
      }
    }
  ]
})
