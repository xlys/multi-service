import Vue from 'vue'
import Router from 'vue-router'
Vue.use(Router)

export default new Router({
  routes: [
    {
      path: '/alarm/manage',
      component: (resolve) => {
        require(['../components/manage.vue'], resolve)
      }
    },
    {
      path: '/alarm/statistics',
      component: (resolve) => {
        require(['../components/statistics.vue'], resolve)
      }
    },
    {
      path: '/alarm',
      component: (resolve) => {
        require(['../components/manage.vue'], resolve)
      }
    },
    {
      path: '/',
      component: (resolve) => {
        require(['../components/manage.vue'], resolve)
      }
    }
  ]
})
