module.exports = {
  devServer: {
    proxy: {
      '/api': {
        target: 'http://localhost:3001',
        changeOrigin: true
      }
    }
  },
  configureWebpack: config => {
    config.output.filename = 'alarm.[name].js'
  },
  assetsDir: "alarm-static",
  baseUrl: "alarm/"
}
