const router = require('koa-router')()

const type = {
    1: '报警输入',
    2: '报警输出',
    3: '移动侦测',
    4: '重点关注'
}
function GetRandomNum(Min,Max){   
    var Range = Max - Min;   
    var Rand = Math.random();   
    return(Min + Math.round(Rand * Range));   
}

router.get('/manage', function (ctx, next) {
    let manageArr = []
    let len = GetRandomNum(10, 20)
    for (let index = 0; index < len; index++) {
        let obj = {
            name: `报警${GetRandomNum(1, 7)}`,
            mainname: `设备${GetRandomNum(1, 10)}`,
            chan: GetRandomNum(1, 100),
            level: GetRandomNum(1, 10),
            alarmTypeName: type[GetRandomNum(1, 4)] || '报警输入',
            alarmtemplate: `2018年12月${GetRandomNum(1, 24)}日`,
            maxdelaytime: GetRandomNum(1, 10) * 100,
            minintervaltime: GetRandomNum(1, 10) * 5
        }
        manageArr.push(obj)
    }
  ctx.body = manageArr
})

router.get('/statistics', function (ctx, next) {
    let statisticsArr = []
    let len = GetRandomNum(10, 20)
    for (let index = 0; index < len; index++) {
        let obj = {
            org: `机构${GetRandomNum(1, 6)}`,
            time: `2018-${GetRandomNum(9, 12)}-${GetRandomNum(1, 24)} ${GetRandomNum(0, 23)}:${GetRandomNum(0, 59)}:${GetRandomNum(1, 59)}`,
            level: GetRandomNum(1, 10),
            type: type[GetRandomNum(1, 4)] || `报警输入`,
            case: GetRandomNum(0, 1) ? '已处理' : '待处理'
        }
        statisticsArr.push(obj)
    }
  ctx.body = statisticsArr
})

module.exports = router
