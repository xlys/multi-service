const router = require('koa-router')()

const resTree = [
  {
      title: '根节点',
      expand: true,
      children: [
          {
              title: '节点 1-1',
              expand: false,
              children: [
                  {
                      title: '设备 1-1-1'
                  },
                  {
                      title: '设备 1-1-2'
                  }
              ]
          },
          {
              title: '节点 1-2',
              expand: false,
              children: [
                  {
                      title: '设备 1-2-1'
                  },
                  {
                      title: '设备 1-2-1'
                  }
              ]
          }
      ]
  }
]

router.get('/orgTree', function (ctx, next) {
  ctx.body = 'this is a orgTree!'
})

router.get('/resTree', function (ctx, next) {
  ctx.body = resTree
})

module.exports = router
