'use strict'
const fs = require('fs')
const path = require('path')
const html = fs.readFileSync(path.resolve(__dirname, '../dist/index.html'), 'utf-8')
const newHtml = html.replace(/static/g, 'auth/static')
fs.writeFileSync(path.join(__dirname, '../dist/index.html'), newHtml)
