import Vue from 'vue'
import Helloworld from '@/components/Helloworld'

describe('Helloworld.vue', () => {
  it('should render correct contents', () => {
    const Constructor = Vue.extend(Helloworld)
    const vm = new Constructor().$mount()
    expect(vm.$el.querySelector('.hello h1').textContent)
      .toEqual('Welcome to Your Vue.js App')
  })
})
